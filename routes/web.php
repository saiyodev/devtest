<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');
Route::post('book-cleaner', 'HomeController@bookCleaner');

Route::resource('city', 'CityController');
Route::resource('customer', 'CustomerController');
Route::resource('booking', 'BookingController');
Route::resource('cleaner', 'CleanerController');

Route::get('/login', 'SessionsController@login');
Route::post('login', 'SessionsController@postLogin');
Route::post('logout', 'SessionsController@logout');

Route::get('/home', 'HomeController@index')->middleware('auth');
