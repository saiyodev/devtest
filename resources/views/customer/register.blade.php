@extends('layouts.frontend')

@section('content')
<!-- Contact Start -->
    <section id="contact" class="section-space-padding">
       <div class="container">
          <!-- <div class="row">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h2>Contact Me.</h2>
                          <div class="divider dark">
                           <i class="icon-envelope-open"></i>
                         </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                    </div>
                </div>
            </div> -->
            
         
         <div class="margin-top-30 margin-bottom-50">
           <div class="row">
           
             <div class="col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8">   
                 
               <div class="row">
                  {!! Form::open(['url' => '/book-cleaner', 'class' => 'form-horizontal', 'files' => true]) !!}
                    
                      <div class="col-sm-12">
                        <div class="form-group">
                          {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                         </div>
                        </div>
                                
                       <div class="col-sm-12">
                        <div class="form-group">
                          {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                         </div>
                        </div>

                        <div class="col-sm-12">
                        <div class="form-group">
                          {!! Form::text('phone_number', null, ['class' => 'form-control', 'placeholder' => 'Phone Number']) !!}
                    {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
                         </div>
                        </div>

                        <div class="col-sm-12">
                        <div class="form-group">
                          <select class='form-control' name="city">
                                <option value="">Select City</option>
                                @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                         </div>
                        </div>

                        <div class="col-sm-12">
                        <div class="form-group">
                          {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Date']) !!}
                            {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
                         </div>
                        </div>
                                
                         <div class="col-sm-12">
                         <div class="form-group">
                          {!! Form::number('no_of_hours', null, ['class' => 'form-control', 'placeholder' => 'No Of Hours']) !!}
                    {!! $errors->first('no_of_hours', '<p class="help-block">:message</p>') !!}
                    </div>
                          </div>
                                
                       
                   
                   
                    <div class="text-center">      
                       <button type="submit" class="button button-style button-style-dark button-style-color-2">Submit</button>
                      </div>
                       
                  {!! Form::close() !!}
                   
                </div>
              </div>
            </div>
           
        
        </div>
       </div>
       @if(session()->has('error_message'))
    <div style="width: 100%; float: left;">
        
        <span class="alert alert-danger">
            {{session()->get('error_message')}}
        </span>
        
    </div>
@endif
@if(session()->has('info_message'))
    <div  style="width: 100%; float: left;">
        
        <span class="alert alert-info">
            {{session()->get('info_message')}}
        </span>
        
    </div>
@endif
     </section>
     <!-- Contact End -->

@endsection
@section('script')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
@endsection