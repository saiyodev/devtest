<!DOCTYPE html>
<html>
<head>
	<title>New Booking</title>
</head>
<body>
Hello {{$booking->cleaner->first_name . ' ' . $booking->cleaner->first_name}},<br><br>
A new booking is made by {{$booking->customer->first_name . ' ' . $booking->customer->last_name}}<br>
for date: {{$booking->date->format('F j,Y') }}.<br>
Customer Information:<br>
<ul>
	<li>First Name: {{$booking->customer->first_name}}</li>
	<li>Last Name: {{$booking->customer->last_name}}</li>
	<li>Phone Number: {{$booking->customer->phone_number}}</li>
	<li>No Of Hours: {{$booking->customer->phone_number}}</li>
</ul>
<br><br>
Regards,<br>
PreHire Team.
</body>
</html>