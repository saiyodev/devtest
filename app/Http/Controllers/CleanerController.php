<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cleaner;
use App\City;
use Illuminate\Http\Request;
use Session;

class CleanerController extends Controller
{
    /**
     * Create a new  controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('is_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $cleaner = Cleaner::paginate(25);
        
        return view('cleaner.index', compact('cleaner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $cities = City::all();
        return view('cleaner.create',  compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'quality_score' => 'required|numeric|min:0.0|max:5.0',
            'email' => 'required|email'
        ]);

        $requestData = $request->all();
        // dd($requestData);
        $cleaner = Cleaner::create($requestData);
        if($request->has('cities'))
        {
            $cleaner->cities()->attach($requestData['cities']);
        }
        Session::flash('flash_message', 'Cleaner added!');

        return redirect('cleaner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cleaner = Cleaner::findOrFail($id);

        return view('cleaner.show', compact('cleaner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cleaner = Cleaner::findOrFail($id);

        $cities = City::all();
        // dd(in_array($cities[0]->city,$cleaner->cities->pluck('id')->toArray()));
        return view('cleaner.edit', compact('cleaner', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
         $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'quality_score' => 'required|numeric|min:0.0|max:5.0',
            'email' => 'required|email'
        ]);
        $requestData = $request->all();
        
        $cleaner = Cleaner::findOrFail($id);
        $cleaner->update($requestData);
        if($request->has('cities'))
        {
            $cleaner->cities()->sync($requestData['cities']);
        }
        // $cleaner->cities()->attach($requestData['cities']);
        Session::flash('flash_message', 'Cleaner updated!');

        return redirect('cleaner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $cleaner = Cleaner::find($id);
        $cleaner->delete();
        $cleaner->cities()->detach();
        Session::flash('flash_message', 'Cleaner deleted!');

        return redirect('cleaner');
    }
}
