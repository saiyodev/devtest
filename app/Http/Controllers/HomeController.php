<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Customer;
use App\Cleaner;
use App\Booking;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewBooking;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        return view('customer.register', compact('cities'));
    }

    public function bookCleaner(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required',
            'city' => 'required',
            'date' => 'required',
            'no_of_hours' => 'required|numeric'
        ]);

        $customer = Customer::where('phone_number', $request->phone_number)->first();
        if(count($customer)<1)
        {
            $customer = Customer::create($request->all());
        }
        $city = City::find($request->city);
        // dd($request->city,$city->cleaners->first());
        if(count($city->cleaners)>0)
        {
            //$city->cleaners->first()
            $booking = Booking::create([
                    'date' => $request->date,
                    'customer_id' => $customer->id,
                    'cleaner_id' => $city->cleaners->first()->id,
                    'city' => $request->city,
                    'no_of_hours' => $request->no_of_hours
                ]);
            Mail::to($booking->cleaner->email)->send(new NewBooking($booking));
            Session::flash('info_message', 'Cleaner ' . $city->cleaners->first()->first_name . ' has been assigned');
        } else {
            Session::flash('error_message', 'No Cleaner is available, so we could not fulfill your request.');
        }
        return back();
    }
}
