<?php

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class CreateRolesTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
        *   First empty table roles its related table.
        **/
        DB::table('roles')->truncate();
        DB::table('role_user')->truncate();
        
        /*
        *Pre defined Roles for website
        */
        DB::table('roles')->insert([
            'name' => 'administrator',
            'label' => 'Site Administrator'
        ]);
        DB::table('roles')->insert([
            'name' => 'customer',
            'label' => 'Site Customer'
        ]);
        
    }
    
     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
