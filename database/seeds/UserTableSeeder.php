<?php

use App\User;
use Carbon\Carbon;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
        *   First epty table users
        **/
        DB::table('users')->truncate();
        
        /**
        *    Default admin for the site.
        **/
        $aId = DB::table('users')->insertGetId([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('adminadmin'),
            'created_at' => Carbon::now()
        ]);
        $user = User::find($aId);
        $user->assignRole('administrator');
    }
}
